<%-- 
    Document   : venta
    Created on : 12-11-2019, 14:27:58
    Author     : fenix
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="page-header">
            <h1>Crear nueva venta</h1>
        </div>
        
        <div class="container">
            <form action="agregar-producto" method="post">
                
                <div class="form-group">
                    <label for="">Agregar al carro</label>
                    <select name="cboProducto" class="btn btn-default" id="cboProducto">
                        <option value="">Seleccionar</option>
                         <!--Recorremos los Productos --> 
                         <c:forEach items="${productos}" var="p">
                             <option value="${p.getId()}">${p.getNombre()}</option>               
                         </c:forEach>
                    </select>
                    
                    <input type="number" name="txtCantidad" class="btn btn-default" id="txtCantidad" placeholder="Cantidad">
                    <input type="submit" class="btn btn-default" value="Agregar">
                </div>

            </form>  
        </div>
        
        <!--mostramos el error , de producto no encontrado-->
        <h4>${mensaje}</h4>
        
         <hr>
        
         <!--mostramos los productos listados-->
        <table class="table table-striped">
            <tr>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Subtotal</th>
                <th>Opciones</th>
            </tr>
            
            <!--recorremos lo que hay en la sesion-->
            <c:forEach items="${sessionScope.carroCompra.getProductos()}" var="p">
                <tr>
                    <td>${p.getNombre()}</td>
                    <td>${p.getPrecio()}</td>
                    <td>${p.getCantidad()}</td>
                    <td>${p.getSubTotal()}</td>
                    <td>
                         <!--pasamos por parametro el producto a eliminar-->
                        <a href="eliminar-producto?id=${p.getId()}">Eliminar</a>
                    </td>
                </tr>
            </c:forEach>
            
        </table>
         
        <div class="container">
            <h3>Total: ${sessionScope.carroCompra.getTotalFinal()}</h3>
        </div>

        <div class="container">
            <form action="finalizar-venta" method="post">
                <div class="form-group">
                    <label for="">Medio de pago</label>
                    
                        <div class="radio">
                            <label><input type="radio" name="medio_pago" id="rbtEfectivo" value="0" checked="">Efectivo</label>
                        </div>
                    
                        <div class="radio">
                            <label><input type="radio" name="medio_pago" id="rbtTarjeta" value="1" >Tarjeta</label>
                        </div>
                    
                    <br>
                    <div class="container">
                        <input type="submit" value="Finalizar venta" class="btn btn-success">
                    </div>
                </div>
            </form>
        </div>
         
    </body>
</html>
