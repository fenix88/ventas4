/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import entidades.Producto;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author fenix
 */

// todas las T van a ser reemplazadas por el nombre del modelo que le enviemos
public abstract class Modelo<T> {
    
    
    private String nombreModelo;
    
    public String getNombreModelo(){
        return nombreModelo;
    }
    
    public List<T> listar(){
    
        List<T> entidades = null;
        Session sesion = HibernateUtil.getSessionFactory().openSession();
        
        try {
            // con el getNombreModelo saco el nombre del modelo que me lo da la herencia
            String hql = "from " + getNombreModelo();
            Query q = sesion.createQuery(hql);
            
            //ejecuicion de la consulta
            entidades = q.list();
            
        } catch (Exception e){
            System.out.println(e.getMessage());
        } finally {
            sesion.close();
        }
        
        return entidades;
    }
    
    // cualquier entidad va a poder agregarse, concepto polimorfico
    public boolean agregar(T entidad){
        
        boolean fueAgregado = false;  
        Session sesion = HibernateUtil.getSessionFactory().openSession();
        
        try {
            //abrir transaccion de hibernate
            Transaction tx = sesion.beginTransaction();
            // hibernate se encarga de realizar el proceso de guardado en la entidad
            sesion.save(entidad);
            tx.commit();
            fueAgregado = true;
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally{
            // cerramos la sesion para dejar de utilizar memoria
            sesion.close();
        }
        return fueAgregado;
    }
    
    
    public boolean modificar(T entidad){
        
        boolean fueModificado = false;
        
        Session sesion = HibernateUtil.getSessionFactory().openSession();
        
        try {
            
            //abrir transaccion de hibernate
            Transaction tx = sesion.beginTransaction();
            sesion.update(entidad);
            tx.commit();
            fueModificado = true;
            
        } catch (Exception e) {
            // si hay error lo mostramos por consola
            System.out.println(e.getMessage());
        } finally{
            sesion.close();
        }
        return fueModificado;
    }
    
    public boolean eliminar(T entidad){
        
        boolean fueEliminado = false;
        
        Session sesion = HibernateUtil.getSessionFactory().openSession();
        
        try {
            
            //abrir transaccion de hibernate
            Transaction tx = sesion.beginTransaction();
            sesion.delete(entidad);
            tx.commit();
            fueEliminado = true;
            
        } catch (Exception e) {
            // si hay error lo mostramos por consola
            System.out.println(e.getMessage());
        } finally{
            sesion.close();
        }
        return fueEliminado;
    }
    
    // metodo para ser reutilizado en todas las tablas
    public T buscarPorId(int id){
        T entidad = null;
        
        Session sesion = HibernateUtil.getSessionFactory().openSession();
        
        try { 
            
           //realizamos consulta a la base de datos tomando cualquier modelo
            // p de producto de la tabla Producto y lo deja con alias p
            // p.id corresponde a la tabla y id a lo que viene por parametro
            String hql = "select p from " +getNombreModelo()+ " p where p.id = :id";
            Query q = sesion.createQuery(hql);
            //pasamo id por parametro a la consulta, evitamos inyecciones sql
            q.setInteger("id", id);
            
            // recibimos la entidad haciendo un casteo a T
            // hacemos la consulta y rescatamos el primer valor
            entidad = (T) q.list().get(0);
            
            
        } catch (Exception e) {
            // si hay error lo mostramos por consola
            System.out.println(e.getMessage());
        } finally{
            sesion.close();
        }
        
        return entidad;
    }
    
    
    public T buscarLogin(String nombreUsuario){
        T entidad = null;
        
        Session sesion = HibernateUtil.getSessionFactory().openSession();
        
        try { 
            
           //realizamos consulta a la base de datos tomando cualquier modelo
           String hql = "select p from " +getNombreModelo()+ " p where p.nombreUsuario = :nombreUsuario ";
            Query q = sesion.createQuery(hql);
            q.setString("nombreUsuario", nombreUsuario);
            //q.setString("pass", pass);
            // recibimos la entidad haciendo un casteo a T
            entidad = (T) q.list().get(0);
            
            
        } catch (Exception e) {
            // si hay error lo mostramos por consola
            System.out.println(e.getMessage());
        } finally{
            sesion.close();
        }
        
        return entidad;
    }
    
    
    
}
