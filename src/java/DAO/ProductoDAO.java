/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import entidades.Producto;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author luis valenzuela
 */

// el Producto va a reemplazar la <T> del Modelo
// le pasamos al Modelo el Producto
public class ProductoDAO extends Modelo<Producto> {

    //sobrescritura de getModelo
    
    @Override
    public String getNombreModelo() {
        
        //mandamos el nombre al modelo,
        // de la entidad de hibernate no de la BD directamente al modelo
        return "Producto";
    }
      
}
