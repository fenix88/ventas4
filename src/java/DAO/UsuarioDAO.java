/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import entidades.Usuario;


/**
 *
 * @author luis valenzuela
 */

// el Producto va a reemplazar la <T> del Modelo
public class UsuarioDAO extends Modelo<Usuario> {

    //sobrescritura de getModelo
    
    @Override
    public String getNombreModelo() {
        
        //mandamos el nombre al modelo
        return "Usuario";
    }
      
}
