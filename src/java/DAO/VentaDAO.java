/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

//import carrocompra.ProductoCarro;
import carrocompra.ProductoCarro;
import entidades.Producto;
import entidades.ProductoVenta;
import entidades.Venta;
import java.util.ArrayList;
import java.util.Date;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author anita
 */

public class VentaDAO extends Modelo<Venta>{

    ProductoVentaDAO pv = new ProductoVentaDAO();
    
    @Override
    public String getNombreModelo() {
        return "Venta";
    }
       
    
    
    
   // insertar una venta y luego guardar todos los prductos relacionados
    //recibe todos los productos del carro
    public Venta procesarVenta(ArrayList<ProductoCarro> productos, int medioPago){
        
        Venta venta = null; 
        
        //obtengo conexion a BD con hibernate
        Session sesion = HibernateUtil.getSessionFactory().openSession();
        
        try {
            //guardamos la venta
            Transaction tx = sesion.beginTransaction();
            // ventaTmp : venta temporal
            Venta ventaTmp = new Venta();
            ventaTmp.setFechaVenta(new Date());
            
            // casteo int a BigInteger
            java.math.BigInteger montoBigInteger = new java.math.BigInteger(String.valueOf(0));
            ventaTmp.setMontoTotal(montoBigInteger);
            
            // casteo int a BigInteger
            java.math.BigInteger medioPagoBigInteger = new java.math.BigInteger(String.valueOf(medioPago));
            ventaTmp.setMedioPago(medioPagoBigInteger);
            
            this.agregar(ventaTmp);
            int monto = 0;
            
            // sesion.save(ventaTmp);
            
            for (ProductoCarro producto : productos) {
                
                ProductoVenta productoVenta = new ProductoVenta();
                
                // casteo int a BigInteger
                java.math.BigInteger productoCantidad = new java.math.BigInteger(String.valueOf(producto.getCantidad()));
                productoVenta.setCantidad(productoCantidad);
                
                productoVenta.setVenta(ventaTmp);
                
                // casteo int a BigInteger
                java.math.BigInteger productoPrecio = new java.math.BigInteger(String.valueOf(producto.getPrecio()));
                productoVenta.setPrecio(productoPrecio);
                
                // casteo int a BigDecimal
                java.math.BigDecimal productoId = new java.math.BigDecimal(String.valueOf(producto.getId()));
                productoVenta.setProducto(new Producto(productoId));
                
                monto+=producto.getSubTotal();
                
                // agregamos a la BD
                pv.agregar(productoVenta);
            }
            
            /*
            for (ProductoCarro producto : productos) {
                
                ProductoVenta productoVenta = new ProductoVenta();
                productoVenta.setCantidad(producto.getCantidad());
                productoVenta.setVenta(ventaTmp);
                productoVenta.setPrecio(producto.getPrecio());
                productoVenta.setProducto(new Producto(producto.getId()));
                monto+=producto.getSubTotal();
                
                // agregamos a la BD
                pv.agregar(productoVenta);
            }
            */
            
            //actualizo la venta
            // casteo int a BigInteger
            java.math.BigInteger montoTotal = new java.math.BigInteger(String.valueOf(monto));
            ventaTmp.setMontoTotal(montoTotal);
            
            this.modificar(ventaTmp);
            
            tx.commit();
            venta = ventaTmp;
            
             
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally  {
             sesion.close();
        }
        
        return venta;
    }
    
    
    
}
