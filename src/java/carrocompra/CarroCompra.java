/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrocompra;

import java.util.ArrayList;

/**
 *
 * Contiene los productos
 */
public class CarroCompra {
    
    private ArrayList<ProductoCarro> productos = new ArrayList<>();

    public ArrayList<ProductoCarro> getProductos() {
        return productos;
    }
    
    //acceso a agregar un producto
    public boolean agregarProducto(ProductoCarro producto){
        //agrego el producto al arreglo
        this.productos.add(producto);
        return true;
    }
    
    //eliminar producto del carro
    public boolean eliminarProducto(int id){
        for (ProductoCarro producto : productos) {
            if (producto.getId() == id) {
                this.productos.remove(producto);
                return true;
            }
        }
        
        return false;
    }
    
    //total final del carro de compra
    public int getTotalFinal(){
        int monto = 0;
        //recorro todos los productos y sumo su subTotal
        for (ProductoCarro producto : productos) {
            monto+=producto.getSubTotal();
        }
        
        return monto;
    }
    
}
