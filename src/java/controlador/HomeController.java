/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author fenix
 */
@Controller
public class HomeController {
    
    //indicamos la ruta a la que debe ingresar el usuario para llegar aca
    @RequestMapping(value="/home",method = RequestMethod.GET)
    public String home(Model model){
        
        // envio de una variable mensaje con su valor
        model.addAttribute("mensaje", "Tienda de alimentos");
        
        return "home";
    }
           
    
}
