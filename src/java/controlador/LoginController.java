/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import DAO.UsuarioDAO;
import entidades.Usuario;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author fenix
 */

@Controller
public class LoginController {
    
    private UsuarioDAO uDAO = new UsuarioDAO();
    
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String login(Model model){
        
        //mostramos el jsp
        return "login";
    }
    
    
    @RequestMapping(value="/validar-login", method = RequestMethod.POST)
    public String validarLogin(Model model, RedirectAttributes ra,
             //@RequestParam: Obtiene algo que despues se envia
             @RequestParam("txtNombreUsuario") String nombreUsuario,
             //@RequestParam("txtPassword") String pass ,
             //inyectamos el HttpRequest
             HttpServletRequest request){
         
        //verificar si existe el usuario 
        //sacamos un usuario de la BD
         Usuario usuario = uDAO.buscarLogin(nombreUsuario);
         
         if (usuario==null) {
             ra.addFlashAttribute("mensaje", "debe ingresar Nombre y Contraseña");
             return "redirect:login";
         }else{
                //redirijo al jsp
                return "redirect:venta";     
         }
         
         /*
        if (usuario==null) {
            ra.addFlashAttribute("mensaje", "debe ingresar Nombre y Contraseña");
             return "redirect:login";
        } else if (condition2) {
             // block of code to be executed if the condition1 is false and condition2 is true
        } else {
             // block of code to be executed if the condition1 is false and condition2 is false
        }
        */

     }
}
