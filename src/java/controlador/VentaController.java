/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import DAO.CategoriaDAO;
import DAO.ProductoDAO;
import DAO.ProductoVentaDAO;
import DAO.VentaDAO;
import carrocompra.CarroCompra;
import carrocompra.ProductoCarro;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import carrocompra.*;
import entidades.Producto;
import entidades.Venta;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.math.BigInteger; 
/**
 *
 * ñps controladores remplazan a los servlet, 
 * por cada tabla realizo un controlador
 */
@Controller
public class VentaController {
    
    private ProductoDAO pDAO = new ProductoDAO();
    private VentaDAO vDAO = new VentaDAO();
    private ProductoVentaDAO pvDAO = new ProductoVentaDAO();
    private CategoriaDAO cDAO = new CategoriaDAO();
    
    
    //metodo privado que me devuelve todo lo del carrocompra
    // le injectamos HttpServletRequest ya que dentro esta la sesion
    private CarroCompra getCarro(HttpServletRequest request){
        
        //resctamos un atributo de la sesion que se llame carroCompra
        CarroCompra carro = (CarroCompra)request.getSession().getAttribute("carroCompra");
        
        // si no existe un carro lo creamos
        if (carro==null) {
            carro = new CarroCompra();
            // guardo en la sesion el carroCompra
            request.getSession().setAttribute("carroCompra", carro);
        }
        
        // si ya existe se recupera
        return carro;
        
    }
    
    
    @RequestMapping(value="/venta", method = RequestMethod.GET)
    public String venta(Model model){
        
        // una ves que llega la peticion enviamos todos los Productos de la BD
        // a los jsp mediante la variable "productos"
        model.addAttribute("productos", pDAO.listar());
        
        
        //mostramos el jsp
        return "venta";
    }
    
    
     @RequestMapping(value="/agregar-producto", method = RequestMethod.POST)
     public String agregarProducto(Model model, RedirectAttributes ra,
             //@RequestParam: Obtiene algo que despues se envia
             @RequestParam("txtCantidad") int cantidad,
             @RequestParam("cboProducto") int productoId ,
             //inyectamos el HttpRequest para rescatar la sesion del carro
             HttpServletRequest request){
         
        //verificar si existe el producto 
        //sacamos un producto de la BD
         Producto producto = pDAO.buscarPorId(productoId);
         
         if (producto==null) {
             ra.addFlashAttribute("mensaje", "El producto no existe");
             return "redirect:venta";
         }
         
         //si existe el producto lo agregamos al carro
         ProductoCarro pCarro = new ProductoCarro();
         pCarro.setId(productoId);
         
         pCarro.setNombre(producto.getNombre());
         
         // casteo BigInteger a int
         BigInteger b1 ;
         b1 = producto.getPrecio(); 
         int productoPrecio = b1.intValue(); 
         pCarro.setPrecio(productoPrecio);
         
         //cantidad viene por parametro del usuario
         pCarro.setCantidad(cantidad);
         
         //obtenemos el carro de compra y lo agregamos a la sesion
         CarroCompra carro = this.getCarro(request);
         carro.agregarProducto(pCarro);
         
         //redirijo al jsp
         return "redirect:venta";
     }
     
    @RequestMapping(value="/eliminar-producto", method = RequestMethod.GET)
     public String eliminarProducto(Model model, RedirectAttributes rs,HttpServletRequest request,            
             @RequestParam("id") int id) {
        
        //lo eliminamos desde el carro de compra NO desde la base de datos
        CarroCompra carro = getCarro(request);
        carro.eliminarProducto(id);
         
         return "redirect:venta";
     }
     
    // metodo que procesa finalizar venta
     @RequestMapping(value="/finalizar-venta", method = RequestMethod.POST)
     public String finalizarVenta(Model model, RedirectAttributes ra,
             HttpServletRequest request,
             @RequestParam("medio_pago")int medioPago) {
        
         //obtenemos el carro
        CarroCompra carro = this.getCarro(request);
         
        if (carro.getProductos().size() == 0) {
             ra.addFlashAttribute("mensaje" , "No hay productos agregados al carro");
             return "redirect:venta";
        }
          
         // si hay productos los guardamos rescatados desde el caroo
        Venta venta = vDAO.procesarVenta(carro.getProductos(), medioPago);  
        if (venta==null) {
             ra.addFlashAttribute("mensaje", "ha ocurrido un error con la venta");
             return "redirect:venta";
         }
          
          // enviamos los productos al  boucher
          model.addAttribute("productos", carro.getProductos());
          // sacamos el total del carro
          model.addAttribute("total", carro.getTotalFinal());
          //enviamos toda la venta
          model.addAttribute("venta", venta);
          //reseteamos la sesion, porque ya fue ingresada la venta
          request.getSession().setAttribute("CarroCompra", null);
          // enviamos al jsp voucher
          return "voucher";
          
     }
}
