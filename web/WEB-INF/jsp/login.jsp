<%-- 
    Document   : login
    Created on : 12-feb-2019, 1:04:05
    Author     : kucho
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <title>JSP Page</title>
    </head>
    <body>
        
        <div class="container">
            <h1>Login</h1>
         </div>
        
        <div class="container">
            <form action="validar-login" method="post">
                <table>
                    <tr>
                        <td>
                            <label>Nombre Usuario</label>
                        </td>
                        <td>
                            <input type="text" name="txtNombreUsuario">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>PassWord</label>
                        </td>
                        <td>
                            <input type="password" name="txtPassword">
                        </td>
                    </tr>

                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="submit" name="btnLogin" value="Conectar">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        
        ${requestScope.mensaje}
    </body>
</html>
